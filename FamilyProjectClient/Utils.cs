﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CitizenFX.Core;

namespace FamilyProject.Client
{
    class Utils
    {
        /// <summary>
        /// Log messages to the in-game console.
        /// </summary>
        /// <param name="msg"></param>
        public static void Log(string msg)
        {
            Debug.WriteLine($"[FamilyProject]: {msg}");
        }

        /// <summary>
        /// Log exceptions to the in-game console.
        /// </summary>
        /// <param name="ex"></param>
        public static void Throw(Exception ex)
        {
            Debug.WriteLine($"[FamilyProject Exception]: {ex.Message} | {ex.StackTrace}");
        }

    }
}
