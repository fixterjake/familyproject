﻿using System;
using System.Dynamic;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using Newtonsoft.Json;

namespace FamilyProject.Client
{
    public class Client : BaseScript
    {
        static Client _instance;

        /// <summary>
        /// Returns instance of resource.
        /// </summary>
        /// <returns>client instance</returns>
        public static Client GetInstance()
        {
            return _instance;
        }

        /// <summary>
        /// Constructor for Client singleton.
        /// </summary>
        public Client()
        {
            _instance = this;
            Loader.Init();
        }

        /// <summary>
        /// Registers event handler.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="action"></param>
        public void RegisterEventHandler(string name, Delegate action)
        {
            try
            {
                EventHandlers.Add(name, action);
            }
            catch (Exception ex)
            {
                Utils.Throw(ex);
            }
        }
        
        /// <summary>
        /// Un-registers event handler.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="action"></param>
        public void UnregisterEventHandler(string name, Delegate action)
        {
            try
            {
                EventHandlers.Remove(name);
            }
            catch (Exception ex)
            {
                Utils.Throw(ex);
            }
        }

        /// <summary>
        /// Registers export.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="action"></param>
        public void RegisterExport(string name, Delegate action)
        {
            try
            {
                Exports.Add(name, action);
            }
            catch (Exception ex)
            {
                Utils.Throw(ex);
            }
        }

        /// <summary>
        /// Calls export.
        /// </summary>
        /// <returns>Returns exports</returns>
        public ExportDictionary CallExport()
        {
            try
            {
                return Exports;
            }
            catch (Exception ex)
            {
                Utils.Throw(ex);
                return null;
            }
        }

        /// <summary>
        /// Registers a function as a tick handler.
        /// </summary>
        /// <param name="action"></param>
        public void RegisterTickHandler(Func<Task> action)
        {
            try
            {
                Tick += action;
            }
            catch (Exception ex)
            {
                Utils.Throw(ex);
            }
        }

        /// <summary>
        /// Un-registers function as a tick handler.
        /// </summary>
        /// <param name="action"></param>
        public void UnregisterTickHandler(Func<Task> action)
        {
            try
            {
                Tick -= action;
            }
            catch (Exception ex)
            {
                Utils.Throw(ex);
            }
        }

        /// <summary>
        /// Set NUI focus.
        /// </summary>
        /// <param name="focus"></param>
        /// <param name="cursor"></param>
        public void SetNuiFocus(bool focus, bool cursor)
        {
            API.SetNuiFocus(focus, cursor);
        }

        /// <summary>
        /// Send NUI data.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <param name="data"></param>
        public void SendNuiData(string type, string name, string data = null)
        {
            API.SendNuiMessage(JsonConvert.SerializeObject(new { type, name, data }));
        }

        /// <summary>
        /// Registers NUI callback.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="callback"></param>
        public void RegisterNuiCallback(string type, Action<ExpandoObject, CallbackDelegate> callback)
        {
            API.RegisterNuiCallbackType(type);
            RegisterEventHandler($"__cfx_nui:{type}", callback);
        }

    }
}
