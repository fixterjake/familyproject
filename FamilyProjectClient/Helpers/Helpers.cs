﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.Native;

namespace FamilyProject.Client.Helpers
{
    class Helpers
    {
        /// <summary>
        /// Creates blip
        /// </summary>
        /// <param name="_pos"></param>
        /// <param name="_sprite"></param>
        /// <param name="_color"></param>
        /// <param name="_label"></param>
        /// <param name="_isShortRange"></param>
        /// <param name="_scale"></param>
        /// <returns>Blip or null if exception thrown.</returns>
        public static Blip CreateBlip(Vector3 pos, BlipSprite sprite, BlipColor color, string label, bool isShortRange, float scale = 0.75f)
        {
            try
            {
                Blip b = World.CreateBlip(pos);
                b.Sprite = sprite;
                b.Color = color;
                b.IsShortRange = isShortRange;
                b.Name = label;
                b.Scale = scale;
                return b;
            }
            catch (Exception ex)
            {
                Utils.Throw(ex);
                return null;
            }
        }

        /// <summary>
        /// Inits scaleform
        /// </summary>
        /// <param name="name"></param>
        /// <param name="function"></param>
        /// <param name="main"></param>
        /// <param name="desc"></param>
        /// <returns>Scaleform or 0 if exception is thrown.</returns>
        public static async Task<int> CreateScaleform(string name, string function, string main, string desc)
        {
            try
            {
                var scaleform = API.RequestScaleformMovie(name);
                while (API.HasScaleformMovieLoaded(scaleform))
                {
                    await BaseScript.Delay(10);
                }

                API.PushScaleformMovieFunction(scaleform, function);
                API.PushScaleformMovieFunctionParameterString(main);
                API.PushScaleformMovieMethodParameterString(desc);
                API.PopScaleformMovieFunctionVoid();
                return scaleform;
            }
            catch (Exception ex)
            {
                Utils.Throw(ex);
                return 0;
            }
        }
    }
}
